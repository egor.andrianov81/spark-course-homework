# Spark course homework
## Homework 1. MapReduce sort
Implement Sample Sort without using built-in RDD functions `sortByKey`, `sample` and `repartitionAndSortWithinPartitions` in file RDDSorter.scala. List of all available Spark transformations - https://spark.apache.org/docs/latest/rdd-programming-guide.html#transformations

After completion of this assignment run tests to check sampleSort works correctly by running:
```
sbt "test:testOnly *RDDSorterTest"
```
It is advisable to add your own tests for each function and tests for corner cases.

Also, Main class provides usage example, which orders graph vertices by the number of outgoing edges. Remove `println` to play with bigger graphs. To run it, type:
```
sbt "runMain course.spark.homework1.Main"
```

## Homework 2. Real-time Wikipedia edit log analysis
Write Spark Streaming transformation to compute top 100 Wikipedia categories by number of page edits in real-time. See TODOs in WikipediaEditLoader and WikipediaEditAnalyzer files for details under `src/main/scala/course/spark/homework2` directory.

To start the pipeline locally (this time it consists of two applications, which should run simultaneously), run the following command:
```
sbt "runMain course.spark.homework2.WikipediaEditLoader /tmp/spark"
```
and then in another terminal:
```
sbt "runMain course.spark.homework2.WikipediaEditAnalyzer /tmp/spark"
```
You can use any directory you like instead of `/tmp/spark`, just make sure to use the same base dir.

